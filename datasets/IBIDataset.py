import random
import torch
from torch.utils.data import Dataset, DataLoader


class IBIDataset(Dataset):
    def __init__(self, root_dir, subsample_rate=3):
        self._root_dir = root_dir
        self._subsample_rate = subsample_rate
        self._data = [[random.randint(0, 3) for j in range(100)] for i in range(5000)]

    def __len__(self):
        return len(self._data)

    def _subsample(self, full_signal, proportion):
        return full_signal[::proportion]

    def __getitem__(self, idx):
        Y = self._data[idx]
        X = self._subsample(Y, self._subsample_rate)
        return X, Y
